package sample.config;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.collect.Lists;

import sample.web.dto.Problem;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.AuthorizationCodeGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.TokenEndpoint;
import springfox.documentation.service.TokenRequestEndpoint;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;


public abstract class BaseSwaggerConfig {
	public static final String AUTH_SERVER = "http://localhost:8084/spring-security-oauth-server/oauth";
	public static final String FOO_CLIENT_ID = "fooClientIdPassword";

	public static final String FOO_CLIENT_SECRET = "secret";

	public static final String BAR_CLIENT_ID = "barClientIdPassword";
	public static final String BAR_CLIENT_SECRET = "secret";
	@Bean
	ApiInfo apiInfo()
	{
		return new ApiInfo("Oauth spring swagger examples", 
				"This Oauth examples project can be run in two modes- using regular swagger and using simplified swagger. Use the maven dependency spring-swagger-simplified for automatically creating additional documentation based on validation annotations. Please see Readme.md for details of all the fetures demonstrated.", 
				"1.0.9",
				"Use as you like",
				null,
				"The Apache Software License, Version 2.0",
				"http://www.apache.org/licenses/LICENSE-2.0.txt",
					Collections.EMPTY_LIST
				);
	}
	

	@Bean
	public Docket api() {
		return changeGlobalResponses(new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.any()).paths(PathSelectors.any()).build())
						.securitySchemes(
								Arrays.asList(fooSecurityScheme(), barSecurityScheme(), userReadSecurityScheme()))
						.securityContexts(Arrays.asList(fooRWsecurityContext(), barRWsecurityContext1(), barRWsecurityContext2())
								).apiInfo(apiInfo());
	}

	@Bean
	public SecurityConfiguration security() {
		return SecurityConfigurationBuilder.builder().clientId(FOO_CLIENT_ID).clientSecret(FOO_CLIENT_SECRET)
				.useBasicAuthenticationWithAccessCodeGrant(true).build();
	}

	private Docket changeGlobalResponse(Docket docket, RequestMethod requestMethod, HttpStatus httpStatus,
			String message, String className) {
		return docket.globalResponseMessage(requestMethod, Lists.newArrayList(new ResponseMessageBuilder()
				.code(httpStatus.value()).message(message).responseModel(new ModelRef(className)).build()));
	}

	private Docket changeGlobalResponses(Docket docket) {
		docket = docket.useDefaultResponseMessages(false);
		docket = changeGlobalResponse(docket, RequestMethod.GET, HttpStatus.INTERNAL_SERVER_ERROR,
				"Server error Try again later", classToName(Problem.class));
		docket = changeGlobalResponse(docket, RequestMethod.POST, HttpStatus.INTERNAL_SERVER_ERROR,
				"Server error Try again later", classToName(Problem.class));
		docket = changeGlobalResponse(docket, RequestMethod.POST, HttpStatus.INTERNAL_SERVER_ERROR,
				"Server error Try again later", classToName(Problem.class));
		docket = changeGlobalResponse(docket, RequestMethod.DELETE, HttpStatus.INTERNAL_SERVER_ERROR,
				"Server error Try again later", classToName(Problem.class));
		return docket;
	}

	protected abstract String classToName(Class clazz);

	private SecurityScheme fooSecurityScheme() {
		GrantType grantType = new AuthorizationCodeGrantBuilder()
				.tokenEndpoint(new TokenEndpoint(AUTH_SERVER + "/token", "oauthtoken")).tokenRequestEndpoint(
						new TokenRequestEndpoint(AUTH_SERVER + "/authorize", FOO_CLIENT_ID, FOO_CLIENT_ID))
				.build();

		SecurityScheme oauth = new OAuthBuilder().name("foo_r_w").grantTypes(Arrays.asList(grantType))
				.scopes(Arrays.asList(fooReadWriteScopes())).build();
		return oauth;
	}

	private SecurityScheme barSecurityScheme() {
		GrantType grantType = new AuthorizationCodeGrantBuilder()
				.tokenEndpoint(new TokenEndpoint(AUTH_SERVER + "/token", "oauthtoken")).tokenRequestEndpoint(
						new TokenRequestEndpoint(AUTH_SERVER + "/authorize", BAR_CLIENT_ID, BAR_CLIENT_ID))
				.build();

		SecurityScheme oauth = new OAuthBuilder().name("bar_r_w").grantTypes(Arrays.asList(grantType))
				.scopes(Arrays.asList(barReadWriteScopes())).build();
		return oauth;
	}

	private SecurityContext fooRWsecurityContext() {
		return SecurityContext.builder()
				.securityReferences(Arrays.asList(new SecurityReference("foo_r_w", fooReadWriteScopes())))
				.forPaths(PathSelectors.regex("/foos.*")).build();
	}

	private SecurityContext barRWsecurityContext1() {
		return SecurityContext.builder()
				.securityReferences(Arrays.asList(new SecurityReference("bar_r_w", barReadWriteScopes())))
				.forPaths(PathSelectors.ant("/bars/*"))
				.build();
	}
	
	private SecurityContext barRWsecurityContext2() {
		return SecurityContext.builder()
				.securityReferences(Arrays.asList(new SecurityReference("bar_r_w", barReadWriteScopes())))
				.forPaths(PathSelectors.ant("/bars"))
				.build();
	}
	
	

	private AuthorizationScope[] barReadWriteScopes() {
		AuthorizationScope[] scopes = { new AuthorizationScope("read", "for read operations"),
				new AuthorizationScope("write", "for write operations"),
				new AuthorizationScope("bar", "Access bar API") };
		return scopes;
	}

	private AuthorizationScope[] fooReadWriteScopes() {
		AuthorizationScope[] scopes = { new AuthorizationScope("read", "for read operations"),
				new AuthorizationScope("write", "for write operations"),
				new AuthorizationScope("foo", "Access foo API") };
		return scopes;
	}

	private SecurityScheme userReadSecurityScheme() {
		GrantType grantType = new AuthorizationCodeGrantBuilder()
				.tokenEndpoint(new TokenEndpoint(AUTH_SERVER + "/token", "oauthtoken")).tokenRequestEndpoint(
						new TokenRequestEndpoint(AUTH_SERVER + "/authorize", FOO_CLIENT_ID, FOO_CLIENT_ID))
				.build();

		SecurityScheme oauth = new OAuthBuilder().name("user_r").grantTypes(Arrays.asList(grantType))
				.scopes(Arrays.asList(userReadScopes())).build();
		return oauth;
	}

	/*
	 * private SecurityContext userReadsecurityContext() { return
	 * SecurityContext.builder() .securityReferences(Arrays.asList(new
	 * SecurityReference("user_r", userReadScopes())))
	 * .forPaths(PathSelectors.regex("/users/extra")).build(); }
	 */

	private AuthorizationScope[] userReadScopes() {
		AuthorizationScope[] scopes = { new AuthorizationScope("read", "for read operations"), };
		return scopes;
	}

}
