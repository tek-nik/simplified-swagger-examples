package sample.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Foo {
	@NotNull
    private long id;
	@NotNull
	@NotBlank
    private String name;

    public Foo() {
        super();
    }

    public Foo(final long id, final String name) {
        super();

        this.id = id;
        this.name = name;
    }

    //

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}