package sample.web.dto;

public class Problem {
private String logRef;
public Problem(String logRef, String message) {
	super();
	this.logRef = logRef;
	this.message = message;
}



private String message;

public Problem() {
	super();
	
}



public String getLogRef() {
	return logRef;
}

public void setLogRef(String logRef) {
	this.logRef = logRef;
}



public String getMessage() {
	return message;
}



public void setMessage(String message) {
	this.message = message;
}
}
