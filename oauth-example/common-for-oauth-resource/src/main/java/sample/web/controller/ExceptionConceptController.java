package sample.web.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sample.web.dto.Foo;

@RestController

public class ExceptionConceptController {

	@RequestMapping(value = "/exceptionconcept1/{id}", method = RequestMethod.GET)

	public Foo exception1(@PathVariable("id") String id) throws Exception {
		if (true) {
			// forcing a checked exception
			// could also demo with any subclass
			throw new Exception("testing with exception");
		}
		Foo foo = new Foo();
		return foo;
	}

	@RequestMapping(value = "/exceptionconcept2/{id}", method = RequestMethod.GET)

	public Foo exception2(@PathVariable("id") String id) {
		if (true) {
			// forcing a checked exception
			// could also demo with any subclass
			throw new RuntimeException("testing with exception");
		}
		Foo foo = new Foo();
		return foo;
	}

}