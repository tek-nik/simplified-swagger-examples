package sample.config;

import org.springframework.context.annotation.Configuration;

import sample.config.BaseSwaggerConfig;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig{

	@Override
	protected String classToName(Class clazz) {
		
		return clazz.getName();
	}
	
	
	
}
