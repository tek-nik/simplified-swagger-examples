package hello;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Predicates;
import com.google.common.collect.Lists;

import hello.problem.Problem;
import hello.ver.VerConstant;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	ApiInfo apiInfo()
	{
		return new ApiInfo("Extra spring swagger examples", 
				"This extra examples project can be run in two modes- using regular swagger and using simplified swagger. Use the maven dependency spring-swagger-simplified for automatically creating additional documentation based on validation annotations. Please see Readme.md for details of all the fetures demonstrated.", 
				VerConstant.VERSION,
				"Use as you like",
				null,
				"The Apache Software License, Version 2.0",
				"http://www.apache.org/licenses/LICENSE-2.0.txt",
					Collections.EMPTY_LIST
				);
	}
	@Bean
	public Docket api() {
		
		return 
				changeGlobalResponses(new Docket(DocumentationType.SWAGGER_2)
					.select()
				.apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
				.paths(PathSelectors.any())
				
				.build())
				.securitySchemes(Arrays.asList(apiKey(), apiKey1(), apiKey2(), apiKey3()))
		        .securityContexts(Arrays.asList(securityContextd(),securityContextp() ))
		        .apiInfo(apiInfo())
		        ;
				
			
	}
	
	private Docket changeGlobalResponses(Docket docket)
	{
		
		return docket.useDefaultResponseMessages(false)
				.globalResponseMessage(
                        RequestMethod.GET,
                        Lists.newArrayList(
                                new ResponseMessageBuilder()
                                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                        .message("Server error Try again later")
                                       
                                        .responseModel(new ModelRef(Problem.class.getSimpleName()))//Problem.class.getName() will also cause problem
                                        .build()
                        )
                )
                .globalResponseMessage(
                        RequestMethod.POST,
                        Lists.newArrayList(
                                new ResponseMessageBuilder()
                                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                        .message("Server error Try again later")
                                        .responseModel(new ModelRef("string"))
                                        .build()
                        )
                ).globalResponseMessage(
                        RequestMethod.PUT,
                        Lists.newArrayList(
                                new ResponseMessageBuilder()
                                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                        .message("Server error Try again later")
                                        .responseModel(new ModelRef("string"))
                                        .build()
                        )
                ).globalResponseMessage(
                        RequestMethod.DELETE,
                        Lists.newArrayList(
                                new ResponseMessageBuilder()
                                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                        .message("Server error Try again later")
                                        .responseModel(new ModelRef("string"))
                                        .build()
                        )
                );
	}

	private SecurityContext securityContextp() {
	    return SecurityContext.builder()

	        .securityReferences(defaultAuth1())
	        .forPaths(PathSelectors.ant("/pqr*"))
	        .build();
	  }
	
	private SecurityContext securityContextd() {
	    return SecurityContext.builder()
	        .securityReferences(defaultAuth())
	        .forPaths(PathSelectors.ant("/def*"))
	        
	        .build();
	  }
	
	private List<SecurityReference> defaultAuth() {
	    AuthorizationScope authorizationScope = new AuthorizationScope("global", "just a description");
	    AuthorizationScope authorizationScope1 = new AuthorizationScope("global1", "just a description");
	    AuthorizationScope[] authorizationScopes = new AuthorizationScope[2];
	    authorizationScopes[0] = authorizationScope;
	    authorizationScopes[1] = authorizationScope1;
	
	    return Arrays.asList(new SecurityReference("Authorization", authorizationScopes), new SecurityReference("Authorization1", authorizationScopes));
	  }
	
	private List<SecurityReference> defaultAuth1() {
	    AuthorizationScope authorizationScope = new AuthorizationScope("globala", "just a description");
	    AuthorizationScope authorizationScope1 = new AuthorizationScope("globalb", "just a description");
	    AuthorizationScope[] authorizationScopes = new AuthorizationScope[2];
	    authorizationScopes[0] = authorizationScope;
	    authorizationScopes[1] = authorizationScope1;
	
	    return Arrays.asList(new SecurityReference("AuthorizationA", authorizationScopes), new SecurityReference("AuthorizationB", authorizationScopes));
	  }


	
	 private ApiKey apiKey() {
	        return new ApiKey("Authorization", "Header_Name", "header_value"); 
	    }
	 
	 private ApiKey apiKey1() {
	        return new ApiKey("Authorization1", "Header_Name1", "header_value1"); 
	    }
	 
	 private ApiKey apiKey2() {
	        return new ApiKey("AuthorizationA", "Header_NameA", "header_value1"); 
	    }
	 
	 private ApiKey apiKey3() {
	        return new ApiKey("AuthorizationB", "Header_NameB", "header_value1"); 
	    }


	
	


}