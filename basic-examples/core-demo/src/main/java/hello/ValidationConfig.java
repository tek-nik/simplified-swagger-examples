package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@Configuration
public class ValidationConfig {
	/**
	 * Usethis only if you are going to use 
	 * @org.springframework.validation.annotation.Validated instead 
	 * of @javax.validation.Valid
	 * @return the methodValidationPostProcessor
	 */
	
	@Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        MethodValidationPostProcessor processor = new MethodValidationPostProcessor();
        return processor;
    }

}
