package hello.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hello.entities.Address;
import hello.entities.Person;
import hello.repo.AddressRepository;
import hello.repo.PersonRepository;

@Service
public class PersonService {
	@Autowired
	PersonRepository personRepository;
	@Autowired
	AddressRepository addressRepository;
	public Person save(Person person)
	{
		Address savedAddress = addressRepository.save(person.getAddress());
		person.setAddress(savedAddress);
		hello.entities.Person saved = personRepository.save(person);
	
		return saved;
	}
	
	public List<hello.entities.Person> list()
	{
		return (List<hello.entities.Person>) personRepository.findAll();
	}

}
