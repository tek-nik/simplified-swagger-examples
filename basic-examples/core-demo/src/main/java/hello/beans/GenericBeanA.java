package hello.beans;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="genericBean")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenericBeanA<E, F> {

	
	@Valid
	
	private E field1;
	@Valid
	
	private F field2;
	
	public E getField1() {
		return field1;
	}
	public void setField1(E field1) {
		this.field1 = field1;
	}
	public F getField2() {
		return field2;
	}
	public void setField2(F field2) {
		this.field2 = field2;
	}
	
	private GenericBean1<String, Integer> genericField1;

	public GenericBean1<String, Integer> getGenericField1() {
		return genericField1;
	}

	public void setGenericField1(GenericBean1<String, Integer> genericField1) {
		this.genericField1 = genericField1;
	}
	
	@Valid
	
	private F[] field3;
	
	public F[] getField3() {
		return field3;
	}


	public void setField3(F[] field3) {
		this.field3 = field3;
	}
	
	
	@Valid
	private GenericBean1<String, Integer>[] genericArray;

	public GenericBean1<String, Integer>[] getGenericArray() {
		return genericArray;
	}

	public void setGenericArray(GenericBean1<String, Integer>[] genericArray) {
		this.genericArray = genericArray;
	}
	
	
	
	
	private GenericBean1<E, F> genericField2;

	public GenericBean1<E, F> getGenericField2() {
		return genericField2;
	}

	public void setGenericField2(GenericBean1<E, F> genericField2) {
		this.genericField2 = genericField2;
	}
	
	
	@Valid
	private GenericBean1<E, F>[] genericArray1;

	public GenericBean1<E, F>[] getGenericArray1() {
		return genericArray1;
	}
	public void setGenericArray1(GenericBean1<E, F>[] genericArray1) {
		this.genericArray1 = genericArray1;
	}
	
	


	/**/
	
}
