package hello.beans;

import java.util.Map;

public class BeanWithAMapProperty<K, V> {
	
	private String field1;
	private Map<K, V> field2;
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public Map<K, V> getField2() {
		return field2;
	}
	public void setField2(Map<K, V> field2) {
		this.field2 = field2;
	}

}
