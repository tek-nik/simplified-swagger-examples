package hello.beans;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="genericBean")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenericBean<E, F> {

	
	
	public GenericBean(E field1, F field2) {
		super();
		this.field1 = field1;
		this.field2 = field2;
	}
	
	
	public GenericBean() {
		super();
		
	}
	@Valid
	
	private E field1;
	@Valid
	
	private F field2;
	private byte byteField;;
	private Byte byteWrapperField;

	public Byte getByteWrapperField() {
		return byteWrapperField;
	}

	public void setByteWrapperField(Byte byteWrapperField) {
		this.byteWrapperField = byteWrapperField;
	}
	
	public byte getByteField() {
		return byteField;
	}


	public void setByteField(byte byteField) {
		this.byteField = byteField;
	}

private byte[] data;
	
	private Byte[] wdata;

	public Byte[] getWdata() {
		return wdata;
	}

	public void setWdata(Byte[] wdata) {
		this.wdata = wdata;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	public E getField1() {
		return field1;
	}
	public void setField1(E field1) {
		this.field1 = field1;
	}
	public F getField2() {
		return field2;
	}
	public void setField2(F field2) {
		this.field2 = field2;
	}
	
}
