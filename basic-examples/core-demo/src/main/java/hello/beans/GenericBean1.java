package hello.beans;

import javax.validation.Valid;

public class GenericBean1<E, F> {

	public GenericBean1(E field1, F field2) {
		super();
		this.field1 = field1;
		this.field2 = field2;
	}
	
	
	public GenericBean1() {
		super();
		
	}
	@Valid
	private E field1;
	@Valid
	private F field2;
	
	public E getField1() {
		return field1;
	}
	public void setField1(E field1) {
		this.field1 = field1;
	}
	public F getField2() {
		return field2;
	}
	public void setField2(F field2) {
		this.field2 = field2;
	}
	
}
