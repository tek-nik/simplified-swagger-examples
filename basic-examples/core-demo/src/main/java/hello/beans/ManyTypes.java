package hello.beans;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;


public class ManyTypes {
	
	
	
	private URI url;
	public URI getUrl() {
		return url;
	}

	public void setUrl(URI url) {
		this.url = url;
	}

	private byte[] data;
	
	private Byte[] wdata;

	public Byte[] getWdata() {
		return wdata;
	}

	public void setWdata(Byte[] wdata) {
		this.wdata = wdata;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
	private byte byteField;
	private Byte byteWrapperField;

	public Byte getByteWrapperField() {
		return byteWrapperField;
	}

	public void setByteWrapperField(Byte byteWrapperField) {
		this.byteWrapperField = byteWrapperField;
	}

	public byte getByteField() {
		return byteField;
	}

	public void setByteField(byte byteField) {
		this.byteField = byteField;
	}
	
	private String stringField;
	private int intField;
	private int[] intArrayField;
	private List<Integer> integerListField;

	private List<Address> addressesListField;

	private Address[] addressesArrayField;
	private Integer[] integerArrayField;
	private long longField;
	private long[] longArrayField;
	private Long[] longWrapperArrayField;
	private Set<Long> longSetField;
	
	private float floatField;
	private double doubleField;
	private float[] floatArrayField;
	private double[] doublearrayField;
	private Float floatWrapperField;
	private Double doubleWrapperField;
	private List<Float> floatListField;
	private Set<Double> doubleSetField;
	
	private BigInteger bigIntegerField;
	private BigDecimal bigDecimalField;
	private BigInteger[] bigIntegerArrayField;
	private BigDecimal[] bigDecimalArrayField;
	private List<BigInteger> bigIntegerListField;
	private Set<BigDecimal> bigDecimalSetField;
	
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Set<LocalDateTime> localDateTimeSetField;
	private Date dateField= new Date();
	private java.sql.Date sqlDateField;
	private java.sql.Time sqlTimeField;
	private java.sql.Timestamp sqlTimestampField;
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	
	private  LocalDateTime localDateTimeField;
	private java.time.LocalTime localTimeField;
	private java.time.LocalDate localDateField;
	
	private Integer integerWrapperField;
	
	private char charField;
	private char[] charArrayField;
	private Character characterField;
	private Character[] characterArrayField;
	private List<Character> characterListField;
	
	public String getStringField() {
		return stringField;
	}
	public void setStringField(String stringField) {
		this.stringField = stringField;
	}
	public int getIntField() {
		return intField;
	}
	public void setIntField(int intField) {
		this.intField = intField;
	}
	public long getLongField() {
		return longField;
	}
	public void setLongField(long longField) {
		this.longField = longField;
	}
	public Date getDateField() {
		return dateField;
	}
	public void setDateField(Date dateField) {
		this.dateField = dateField;
	}
	public java.sql.Date getSqlDateField() {
		return sqlDateField;
	}
	public void setSqlDateField(java.sql.Date sqlDateField) {
		this.sqlDateField = sqlDateField;
	}
	public LocalDateTime getLocalDateTimeField() {
		return localDateTimeField;
	}
	public void setLocalDateTimeField(LocalDateTime localDateTimeField) {
		this.localDateTimeField = localDateTimeField;
	}
	public java.time.LocalTime getLocalTimeField() {
		return localTimeField;
	}
	public void setLocalTimeField(java.time.LocalTime localTimeField) {
		this.localTimeField = localTimeField;
	}
	public java.time.LocalDate getLocalDateField() {
		return localDateField;
	}
	public void setLocalDateField(java.time.LocalDate localDateField) {
		this.localDateField = localDateField;
	}
	public java.sql.Time getSqlTimeField() {
		return sqlTimeField;
	}
	public void setSqlTimeField(java.sql.Time sqlTimeField) {
		this.sqlTimeField = sqlTimeField;
	}
	public java.sql.Timestamp getSqlTimestampField() {
		return sqlTimestampField;
	}
	public void setSqlTimestampField(java.sql.Timestamp sqlTimestampField) {
		this.sqlTimestampField = sqlTimestampField;
	}
	public Integer getIntegerWrapperField() {
		return integerWrapperField;
	}
	public void setIntegerWrapperField(Integer integerWrapperField) {
		this.integerWrapperField = integerWrapperField;
	}
	public char getCharField() {
		return charField;
	}
	public void setCharField(char charField) {
		this.charField = charField;
	}
	public Character getCharacterField() {
		return characterField;
	}
	public void setCharacterField(Character characterField) {
		this.characterField = characterField;
	}
	public int[] getIntArrayField() {
		return intArrayField;
	}
	public void setIntArrayField(int[] intArrayField) {
		this.intArrayField = intArrayField;
	}
	public Integer[] getIntegerArrayField() {
		return integerArrayField;
	}
	public void setIntegerArrayField(Integer[] integerArrayField) {
		this.integerArrayField = integerArrayField;
	}
	public long[] getLongArrayField() {
		return longArrayField;
	}
	public void setLongArrayField(long[] longArrayField) {
		this.longArrayField = longArrayField;
	}
	public Long[] getLongWrapperArrayField() {
		return longWrapperArrayField;
	}
	public void setLongWrapperArrayField(Long[] longWrapperArrayField) {
		this.longWrapperArrayField = longWrapperArrayField;
	}
	public char[] getCharArrayField() {
		return charArrayField;
	}
	public void setCharArrayField(char[] charArrayField) {
		this.charArrayField = charArrayField;
	}
	public Character[] getCharacterArrayField() {
		return characterArrayField;
	}
	public void setCharacterArrayField(Character[] characterArrayField) {
		this.characterArrayField = characterArrayField;
	}
	public List<Integer> getIntegerListField() {
		return integerListField;
	}
	public void setIntegerListField(List<Integer> integerListField) {
		this.integerListField = integerListField;
	}
	
	public Set<Long> getLongSetField() {
		return longSetField;
	}
	public void setLongSetField(Set<Long> longSetField) {
		this.longSetField = longSetField;
	}
	public List<Character> getCharacterListField() {
		return characterListField;
	}
	public void setCharacterListField(List<Character> characterListField) {
		this.characterListField = characterListField;
	}
	public List<Address> getAddressesListField() {
		return addressesListField;
	}
	public void setAddressesListField(List<Address> addressesListField) {
		this.addressesListField = addressesListField;
	}
	public Address[] getAddressesArrayField() {
		return addressesArrayField;
	}
	public void setAddressesArrayField(Address[] addressesArrayField) {
		this.addressesArrayField = addressesArrayField;
	}
	public Set<LocalDateTime> getLocalDateTimeSetField() {
		return localDateTimeSetField;
	}
	public void setLocalDateTimeSetField(Set<LocalDateTime> localDateTimeSetField) {
		this.localDateTimeSetField = localDateTimeSetField;
	}
	public BigInteger getBigIntegerField() {
		return bigIntegerField;
	}
	public void setBigIntegerField(BigInteger bigIntegerField) {
		this.bigIntegerField = bigIntegerField;
	}
	public BigDecimal getBigDecimalField() {
		return bigDecimalField;
	}
	public void setBigDecimalField(BigDecimal bigDecimalField) {
		this.bigDecimalField = bigDecimalField;
	}
	public float getFloatField() {
		return floatField;
	}
	public void setFloatField(float floatField) {
		this.floatField = floatField;
	}
	public double getDoubleField() {
		return doubleField;
	}
	public void setDoubleField(double doubleField) {
		this.doubleField = doubleField;
	}
	public float[] getFloatArrayField() {
		return floatArrayField;
	}
	public void setFloatArrayField(float[] floatArrayField) {
		this.floatArrayField = floatArrayField;
	}
	public double[] getDoublearrayField() {
		return doublearrayField;
	}
	public void setDoublearrayField(double[] doublearrayField) {
		this.doublearrayField = doublearrayField;
	}
	public Float getFloatWrapperField() {
		return floatWrapperField;
	}
	public void setFloatWrapperField(Float floatWrapperField) {
		this.floatWrapperField = floatWrapperField;
	}
	public Double getDoubleWrapperField() {
		return doubleWrapperField;
	}
	public void setDoubleWrapperField(Double doubleWrapperField) {
		this.doubleWrapperField = doubleWrapperField;
	}
	public List<Float> getFloatListField() {
		return floatListField;
	}
	public void setFloatListField(List<Float> floatListField) {
		this.floatListField = floatListField;
	}
	public Set<Double> getDoubleSetField() {
		return doubleSetField;
	}
	public void setDoubleSetField(Set<Double> doubleSetField) {
		this.doubleSetField = doubleSetField;
	}
	public BigInteger[] getBigIntegerArrayField() {
		return bigIntegerArrayField;
	}
	public void setBigIntegerArrayField(BigInteger[] bigIntegerArrayField) {
		this.bigIntegerArrayField = bigIntegerArrayField;
	}
	public BigDecimal[] getBigDecimalArrayField() {
		return bigDecimalArrayField;
	}
	public void setBigDecimalArrayField(BigDecimal[] bigDecimalArrayField) {
		this.bigDecimalArrayField = bigDecimalArrayField;
	}
	public List<BigInteger> getBigIntegerListField() {
		return bigIntegerListField;
	}
	public void setBigIntegerListField(List<BigInteger> bigIntegerListField) {
		this.bigIntegerListField = bigIntegerListField;
	}
	public Set<BigDecimal> getBigDecimalSetField() {
		return bigDecimalSetField;
	}
	public void setBigDecimalSetField(Set<BigDecimal> bigDecimalSetField) {
		this.bigDecimalSetField = bigDecimalSetField;
	}
	

}
