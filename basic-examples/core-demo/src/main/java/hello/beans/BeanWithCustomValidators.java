package hello.beans;

import customvalidators.DateTimeType;
import customvalidators.LocalDateTimeFormat;
import customvalidators.ValidCrossField;
@ValidCrossField
public class BeanWithCustomValidators {
	@LocalDateTimeFormat(pattern = "yyyyMMddHHmmss",   dateTimeType=DateTimeType.DateTime, message = "Invalid dateTimeField Format. It Should be in yyyyMMddHHmmss format")
	private String dateTimeField;
	@LocalDateTimeFormat(pattern = "yyyyMMdd",   dateTimeType=DateTimeType.Date, message = "Invalid dateTimeField Format. It Should be in yyyyMMdd format")
	private String dateField;
	private String email;
	private String phone;
	public String getDateTimeField() {
		return dateTimeField;
	}
	public void setDateTimeField(String dateTimeField) {
		this.dateTimeField = dateTimeField;
	}
	public String getDateField() {
		return dateField;
	}
	public void setDateField(String dateField) {
		this.dateField = dateField;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	

}
