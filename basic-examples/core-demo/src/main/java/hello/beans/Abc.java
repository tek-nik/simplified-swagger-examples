package hello.beans;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import hello.beans1.TitleEnum;
@XmlRootElement(name="abc")
@XmlAccessorType(XmlAccessType.FIELD) 
public class Abc {
	
	private String field1;
	private int field2;
	private Integer field3;
	@NotNull
	private TitleEnum field4;
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public int getField2() {
		return field2;
	}
	public void setField2(int field2) {
		this.field2 = field2;
	}
	public Integer getField3() {
		return field3;
	}
	public void setField3(Integer field3) {
		this.field3 = field3;
	}
	public TitleEnum getField4() {
		return field4;
	}
	public void setField4(TitleEnum field4) {
		this.field4 = field4;
	}

}
