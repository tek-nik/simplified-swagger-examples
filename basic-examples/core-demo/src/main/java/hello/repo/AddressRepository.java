package hello.repo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hello.entities.Address;


@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {

	

}
