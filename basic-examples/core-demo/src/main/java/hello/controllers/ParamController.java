package hello.controllers;

import java.net.URI;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.ManyTypes;
import hello.beans.Person;
@RestController
public class ParamController {
	
	
	@RequestMapping(value = "/simpleret", method = RequestMethod.GET,  
			produces= {MediaType.TEXT_PLAIN_VALUE})
	
	public String simpleret() {
		
		return "ok";
	}
	
	
	@RequestMapping(value = "/userequest", method = RequestMethod.GET,  
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person userequest(@Valid @NotNull String param1,   HttpServletRequest x, ServletRequest y,  HttpSession s, @Valid @NotNull String param2 ) {
		System.out.println(param1+param2);
		return new Person();
	}
	
	@RequestMapping(value = "/usemap", method = RequestMethod.POST,  consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			produces= {MediaType.TEXT_PLAIN_VALUE})
	/*
	 * A map may be an old mvc model. is also ofc ourse an object
	 * but if it becomes a parameter as in this case
	 * its an object without any named or rather pre known properties.
	 * So best to ignore it- Till we have a means in ui to show the fields dynamically
	 * If you want swagger to use maps properly use it with a @RequestBody
	 * 
	 */
	public String usemap(@Valid @NotNull String param1,   Map x) {
		
		return param1;
	}
	
	@RequestMapping(value = "/usemap", method = RequestMethod.GET,  
			produces= {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	/*
	 * A map may be an old mvc model. is also ofc ourse an object
	 * but if it becomes a parameter as in this case
	 * its an object without any named or rather pre known properties.
	 * So best to ignore it- Till we have a means in ui to show the fields dynamically
	 * 
	 */
	public  String usemapget(@Valid @NotNull String param1,   Map x) {
		
		return "ok";
	}
	
	
	
	@RequestMapping(value = "/usemap1", method = RequestMethod.POST,  consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person usemap1(@Valid @NotNull String param1,   Map<String, Integer> x) {
		
		return new Person();
	}
	
	@RequestMapping(value = "/usemap2", method = RequestMethod.POST,  consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			produces= {MediaType.TEXT_PLAIN_VALUE})
	/*
	 * A map may be an old mvc model. is also ofc ourse an object
	 * but if it becomes a parameter as in this case
	 * its an object without any named or rather pre known properties.
	 * So best to ignore it- Till we have a means in ui to show the fields dynamically
	 * If you want swagger to use maps properly use it with a @RequestBody
	 * 
	 */
	public String usemap2(@Valid @NotNull String param1,  @RequestParam("x") Map x) {
		
		return param1;
	}
	
	@RequestMapping(value = "/parampost", method = RequestMethod.POST,  consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person paramPost(@Valid @NotNull String param1, @Valid  Person x) {
		return x;
	}
	
	
	
	@RequestMapping(value = "/paramput", method = RequestMethod.PUT,  consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person paramPut(@Valid String param1, @Valid  Person x) {
		return x;
	}
	
	@RequestMapping(value = "/paramget", method = RequestMethod.GET 
			,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person paramGet(@Valid String param1, @Valid  Person x) {
		return x;
	}
	
	@RequestMapping(value = "/paramgetwithuri", method = RequestMethod.GET 
			,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person paramGetwithUril(@Valid URI param1) {
		return new Person();
	}
	
	@RequestMapping(value = "/byteparamget", method = RequestMethod.GET 
			,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public ManyTypes byteParamGet(@Valid byte param1) {
		ManyTypes mt = new ManyTypes();
		mt.setByteField(param1);
		return mt;
	}
	
	@RequestMapping(value = "/wbyteparamget", method = RequestMethod.GET 
			,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public ManyTypes wByteparamGet(@Valid Byte param1) {
		ManyTypes mt = new ManyTypes();
		mt.setByteWrapperField(param1);
		return mt;
	}
	@RequestMapping(value = "/byteArrayParamget", method = RequestMethod.GET 
			,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public ManyTypes byteArrayParamGet(@Valid byte[] param1) {
		ManyTypes mt = new ManyTypes();
		mt.setData(param1);
		return mt;
	}
	
	@RequestMapping(value = "/wbyteArrayParamget", method = RequestMethod.GET 
			,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public ManyTypes wByteArrayParamGet(@Valid Byte[] param1) {
		ManyTypes mt = new ManyTypes();
		mt.setWdata(param1);
		return mt;
	}
	
	@RequestMapping(value = "/paramdelete", method = RequestMethod.DELETE ,  
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person paramDeelete(@Valid String param1, @Valid  Person x) {
		return x;
	}
	
	
}
