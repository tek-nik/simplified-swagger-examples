package hello.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.AnotherBean;
import hello.beans.BeanWithAMapProperty;
import hello.beans.GenericBean;
import hello.beans.GenericBean1;
import hello.beans1.Person;
import io.swagger.annotations.ApiOperation;

@RestController

public class GenericsControllerUsingValid {

	@RequestMapping(value = "/pqr", method = RequestMethod.POST,  consumes= {  MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Person> abc1(@Valid @RequestBody hello.beans1.Person y) {
		
		return new ResponseEntity<hello.beans1.Person>(y, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/pqr1", method = RequestMethod.POST,  consumes= {  MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, Integer> abc2(@Valid @RequestBody hello.beans1.Person y) {
		return new GenericBean<String, Integer> ("ok", 200);
	}
	
	@RequestMapping(value = "/pqr2", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, Byte> abc3(@Valid @RequestBody GenericBean<String, Byte> y) {
		return y;
	}
	
	@RequestMapping(value = "/pqr3", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, String> abc4(@Valid @RequestBody GenericBean<String, String> y) {
		return y;
	}
	
	
	
	
	@RequestMapping(value = "/pqr5", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, hello.beans1.Person> abc5(@Valid @RequestBody GenericBean<String, hello.beans1.Person> y) {
		return y;
	}
	
	@RequestMapping(value = "/pqr6", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, GenericBean1<String, hello.beans1.Person>> abc6(@Valid @RequestBody GenericBean<String, GenericBean1<String, hello.beans1.Person>> y) {
		return y;
	}
	
	
	
	@RequestMapping(value = "/pqr7", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, GenericBean1<String, ?>> abc7(@Valid @RequestBody GenericBean<String, GenericBean1<String, ?>> y) {
		return y;
	}
	
	@RequestMapping(value = "/pqr8", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, ? extends AnotherBean> abc8(@Valid @RequestBody GenericBean<String, ? extends AnotherBean> y) {
		return y;
	}
	
	
	@RequestMapping(value = "/pqr9", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, ? super AnotherBean> abc9(@Valid @RequestBody GenericBean<String, ? super AnotherBean> y) {
		return y;
	}
	
	@ApiOperation( value="/pqr10", notes = "The swagger json gets generated properly. The rest api also works well for  json request. In case of xml requests there is some scope of improvement when using such geneerics input")	
	@RequestMapping(value = "/pqr10", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBean<String, AnotherBean> abc10(@Valid @RequestBody GenericBean<String, AnotherBean> y) {
		return y;
	}
	
	@RequestMapping(value = "/pqr11/{id}", method = RequestMethod.POST)

	public Map<String, Long> getMap(  @PathVariable("id")  String id, @RequestBody Map<String, Long> x) {
	Map<String, Long> ret= new HashMap<>();
	ret.put("check",10l );
	ret.put(id,10l );
		return ret;
	}
	
	
	@RequestMapping(value = "/pqr12/{id}", method = RequestMethod.POST)
	
	public Map<String, AnotherBean> getMap1(  @PathVariable("id")  String id, @RequestBody Map<String, AnotherBean> x) {
		AnotherBean a = new AnotherBean();
		a.setField1(id);
	x.put(id,a );
	
		return x;
	}
	
	
	@RequestMapping(value = "/pqr13/{id}", method = RequestMethod.POST)

	public List<String> getList(  @PathVariable("id")  String id, @RequestBody List<String> x) {
	List<String> ret= new ArrayList<>();
	ret.add("check");
	ret.add(id);
		return ret;
	}
	
	@RequestMapping(value = "/pqr14/{id}", method = RequestMethod.POST)

	public List<AnotherBean> getList1(  @PathVariable("id")  String id, @RequestBody List<AnotherBean> x) {
	
	
		return x;
	}
	
	
	@RequestMapping(value = "/pqr15/{id}", method = RequestMethod.GET)

	public BeanWithAMapProperty<String, Long> getbeanWithMap(  @PathVariable("id")  String id) {
		BeanWithAMapProperty<String, Long> ret= new BeanWithAMapProperty<>();
	ret.setField1("abc");
	Map<String, Long> map= new HashMap<>();
	map.put(id, 1000l);
	ret.setField2(map);
		return ret;
	}
	
	
	
	@RequestMapping(value = "/pqr16/{id}", method = RequestMethod.POST)

	public List<List<Map<String, List<String>>>> getList2( @PathVariable("id")  String id,  @RequestBody List<List<Map<String, List<String>>>> x) {
	return x;
	}
	
	@RequestMapping(value = "/pqr17/{id}", method = RequestMethod.POST)
	public Map<String, Map<String, Map<String, Long>>> getMap(  @RequestBody Map<String, Map<String, Map<String, Long>>> x) {
	return x;
	}
	
	@RequestMapping(value = "/pqr18/{id}", method = RequestMethod.POST)
	public Map<String, Map<String, Long>> getMapa(  @RequestBody Map<String, Map<String, Long>> x) {
	return x;
	}
	

	
}