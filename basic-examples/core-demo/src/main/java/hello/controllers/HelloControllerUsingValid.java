package hello.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.Abc;
import hello.beans.Person;

@RestController
@RequestMapping({"/basea", "basea1"})
public class HelloControllerUsingValid {

	
	@RequestMapping(value = "/abc", method = RequestMethod.POST,  consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person abc(@Valid @RequestBody Person x) {
		return x;
	}
	



	
@RequestMapping(value = "/abc/{id}", method = RequestMethod.GET)

	public Person getPerson(  @PathVariable("id")  String id) {
	Person person= new Person();
		return person;
	}


@RequestMapping(value = "/abcx", method = RequestMethod.POST,  consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
produces= {MediaType.APPLICATION_JSON_VALUE})

public Abc abc(@Valid @RequestBody Abc x) {
return x;
}




}