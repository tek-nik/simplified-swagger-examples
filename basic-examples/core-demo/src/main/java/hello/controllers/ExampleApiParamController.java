package hello.controllers;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans2.ExampleApiParamBean;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

@RestController

/*
 * An attempt is made to ignore (sometimes conditionally ignore) some @ApiParam annotation attributes
 * because the data can be otherwise derived more accurately.
 * See readme.md
 */
public class ExampleApiParamController {
	@RequestMapping(value = "/apiparam", method = RequestMethod.POST, /*consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, */produces = { MediaType.APPLICATION_JSON_VALUE })

	public ExampleApiParamBean apimodelprop(
			@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=false, 
			name="name2", 
			readOnly=false ,
			required=false, 
			type="typex", 
			value="val1")
			@Valid String param1,
			
			@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=false, 
			name="name2", 
			readOnly=false ,
			required=false, 
			type="typex", 
			value="val1")
			@Valid String param2, 
			
			@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=false, 
			name="name2", 
			readOnly=true ,
			required=false, 
			type="typex", 
			value="val1")
			@Valid String param3,
			
			@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=true, 
			name="name2", 
			readOnly=false ,
			required=false, 
			type="typex", 
			value="val1")
			@Valid String param4, 
			
			@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=false, 
			name="name2", 
			readOnly=true ,
			required=false, 
			type="typex", 
			value="val1")
			@NotNull
			@Valid String param5,
			
			@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=true, 
			name="name2", 
			readOnly=false ,
			required=false, 
			type="typex", 
			value="val1")
			@NotNull
			@Valid String param6, 
			
			@Valid  ExampleApiParamBean x) {
		return x;
	}
}
