package hello.controllers;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.Person;

@RestController
@Validated
public class HelloControllerUsingValidated {

	@RequestMapping(value = "/abc2", method = RequestMethod.POST , consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person abc(@Valid @RequestBody Person x) {
		return x;
	}
	
	@RequestMapping(value = "/def2", method = RequestMethod.POST)
	
	public Person def( @Size(min = 2) @RequestParam("def") String def) {
		Person person= new Person();
		person.setFirstName(def);
	return person;
	}



	@RequestMapping(value = "/abc2/{id}", method = RequestMethod.GET)

	public Person getPerson(  @NotNull @Size(max=2) @PathVariable("id")  String id) {
	Person person= new Person();
		return person;
	}
	
	@RequestMapping(value = "/ghi", method = RequestMethod.POST)

	public Person ghi( @Valid @RequestBody Person x, /*@NotNull @Size(max=2) */@RequestParam("id")  String id) {
	Person person= new Person();
		return person;
	}
	
	@RequestMapping(value = "/diffParameterTypes/{id}", method = RequestMethod.GET)
	public Person diffParameterTypes( @Size(min=2)  @PathVariable("id") String id,  @Size(min=3) @RequestParam("abc") String abc, @Size(min=4) @RequestHeader("def") String def ) {
		Person person = new Person();
		return person;
	}

}