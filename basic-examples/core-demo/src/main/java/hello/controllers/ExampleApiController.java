package hello.controllers;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans2.ExampleApiParamBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController

/*
 * An attempt is made to ignore (sometimes conditionally ignore) some @Api annotation attributes
 * because the data can be otherwise derived more accurately.
 * See readme.md
 */
@Api(basePath="ignoreit", consumes="ignoreletsusewhatspringgives", description="no this description is deprecated",
hidden=false, position=1, value="yes this description can be used", produces="ignoreletsusewhatspringgives", 
protocols="protocols should be for whole app.lets ignore for now",
 tags= {"tagA", "tagB"}, authorizations = { @Authorization(value="Authorization1") }
		)
public class ExampleApiController {
	@RequestMapping(value = "/apisample", method = RequestMethod.POST, /*consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, */produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "dummy", authorizations = { @Authorization(value="AuthorizationA") })
	public ExampleApiParamBean apisample(
			
			@Valid String param1,
			
			
			@Valid String param2, 
			
			
			@Valid String param3,
			
			
			@Valid String param4, 
			
	
			@NotNull
			@Valid String param5,
			
		
			@NotNull
			@Valid String param6, 
			
			@Valid  ExampleApiParamBean x) {
		return x;
	}
	
	@RequestMapping(value = "/apisample", method = RequestMethod.GET, /*consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, */produces = { MediaType.APPLICATION_JSON_VALUE })

	public ExampleApiParamBean apisample1(
			
			@Valid String param1,
			
			
			@Valid String param2, 
			
			
			@Valid String param3,
			
			
			@Valid String param4, 
			
	
			@NotNull
			@Valid String param5,
			
		
			@NotNull
			@Valid String param6, 
			
			@Valid  ExampleApiParamBean x) {
		return x;
	}
}
