package hello.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/upload")

public class FileUploadController {
	@PostMapping

	public ResponseEntity<String> uploadFile(

			@NotNull @RequestPart("file") MultipartFile file) throws IOException {
		String message=getMessage(file);
		return new ResponseEntity<String>(message, HttpStatus.OK);
	}

	
	@PostMapping(path = "one", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> uploadFile1(

			@NotNull @RequestPart("file1") MultipartFile file1, @NotNull @RequestPart("file2") MultipartFile file2) throws IOException {
		String message="For File1 ("+getMessage(file1)+")";
		message+=",For File2 ("+getMessage(file2)+")";
		return new ResponseEntity<String>(message, HttpStatus.OK);
	}

	@PostMapping(path = "two", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation("This one's file upload does not work from swagger but its backend works. The regular parameter works. Visit http://localhost:[port]/api/fileuploadcheck.html.")
	public ResponseEntity<String> uploadFile2(

			@NotNull @Size(min = 1 ) @RequestPart("files") MultipartFile[] files, @RequestParam("param1") String param1,
			@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") @RequestParam("param2") Date[] param2) throws IOException {
		String message="";
		for (int i = 0; i < files.length; i++) 
		{
			message+=",For files["+i+"] ("+getMessage(files[i])+")";
		}
		message+=",param1="+param1;
		message+=",param2="+Arrays.toString(param2);

		return new ResponseEntity<String>(message, HttpStatus.OK);
	}
	
	private String getMessage(MultipartFile file) throws IOException {
		return "Got file of length="+file.getBytes().length+" ,name="+file.getName()+",originalName="+file.getOriginalFilename()+",size="+file.getSize();
	}

}