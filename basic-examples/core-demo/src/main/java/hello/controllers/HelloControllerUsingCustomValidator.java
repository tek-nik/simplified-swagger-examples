package hello.controllers;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.BeanWithCustomValidators;
import io.swagger.annotations.ApiOperation;

@RestController()
public class HelloControllerUsingCustomValidator {
	@ApiOperation( value="custom validation example", notes = "Use the GET mapping response to obtain a valid POST input. Try changing the date format. Also try removing both email and phone.")	
@PostMapping("/check")
	
	public BeanWithCustomValidators abc(@Valid @RequestBody BeanWithCustomValidators x) {
		return x;
	}
@GetMapping("get")
public BeanWithCustomValidators get() {
	BeanWithCustomValidators x= new BeanWithCustomValidators();
	x.setDateField("20190101");
	x.setDateTimeField("20190101101010");
	x.setEmail("abc@abc.com");
	x.setPhone("123456789");
	return x;
}

}
