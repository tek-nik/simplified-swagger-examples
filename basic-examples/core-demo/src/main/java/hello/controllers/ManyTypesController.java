package hello.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashSet;

import javax.validation.Valid;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.ManyTypes;
import io.swagger.annotations.ApiOperation;

@RestController

public class ManyTypesController {

	@RequestMapping(value = "/many", method = RequestMethod.POST )

	public ManyTypes abc(@Valid @RequestBody ManyTypes x) {
		return x;
	}

	@RequestMapping(value = "/many", method = RequestMethod.GET)

	public ManyTypes abc() throws URISyntaxException {
		ManyTypes manyTypes = new ManyTypes();
		manyTypes.setUrl(new URI("http://example.com"));
		manyTypes.setDateField(new java.util.Date());
		manyTypes.setSqlTimeField(new java.sql.Time(manyTypes.getDateField().getTime()));
		manyTypes.setSqlTimestampField(new java.sql.Timestamp(manyTypes.getDateField().getTime()));
		manyTypes.setLocalDateField(java.time.LocalDate.now());
		manyTypes.setLocalDateTimeField(java.time.LocalDateTime.now());
		manyTypes.setLocalTimeField(java.time.LocalTime.now());
		manyTypes.setIntArrayField(new int[] { 1, 2 });
		manyTypes.setIntegerListField(Arrays.asList(1, 2));
		manyTypes.setIntegerArrayField(new Integer[] { 1, 2 });
		manyTypes.setLongArrayField(new long[] { 1l, 2l });
		HashSet<Long> longSet = new HashSet<Long>();
		Arrays.asList(1l, 2l).addAll(longSet);
		manyTypes.setLongSetField(longSet);
		manyTypes.setCharArrayField(new char[] { 'a', 'b' });
		manyTypes.setCharacterArrayField(new Character[] { 'a', 'b' });
		return manyTypes;
	}

	@RequestMapping(value = "/many1/{id}", method = RequestMethod.GET)

	public ManyTypes getMany1Types(@PathVariable("id") String id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many2/{id}", method = RequestMethod.GET)

	public ManyTypes getMany2Types(@PathVariable("id") long id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many3/{id}", method = RequestMethod.GET)

	public ManyTypes getMany3Types(@PathVariable("id") int id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many4/{id}", method = RequestMethod.GET)
	@ApiOperation( value="getMany4Types summary", notes = "Feed in 2019-12-31T23:40:59")
	public ManyTypes getMany4Types(@PathVariable("id") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") java.util.Date id) {
		ManyTypes manyTypes = new ManyTypes();
		manyTypes.setDateField(id);
		return manyTypes;
	}

	@RequestMapping(value = "/many5/{id}", method = RequestMethod.GET)
	@ApiOperation( value="getMany5Types summary", notes = "Feed in 2019-12-31")
	public ManyTypes getMany5Types(@PathVariable("id") java.sql.Date id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@GetMapping("/many6/{id}")
	
	@ApiOperation( value="getMany6Types summary", notes = "Feed in 2019-12-31T23:40:59")
	public ManyTypes getMany6Types(
			@PathVariable("id") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") java.time.LocalDateTime id) {
		ManyTypes manyTypes = new ManyTypes();
		manyTypes.setDateField(java.util.Date.from(id.atZone(ZoneId.systemDefault()).toInstant()));
		manyTypes.setSqlTimeField(new java.sql.Time(manyTypes.getDateField().getTime()));
		manyTypes.setSqlTimestampField(new java.sql.Timestamp(manyTypes.getDateField().getTime()));
		manyTypes.setLocalDateField(id.toLocalDate());
		manyTypes.setLocalDateTimeField(id);
		manyTypes.setLocalTimeField(id.toLocalTime());
		manyTypes.setIntArrayField(new int[] { 1, 2 });
		manyTypes.setIntegerListField(Arrays.asList(1, 2));
		manyTypes.setIntegerArrayField(new Integer[] { 1, 2 });
		manyTypes.setLongArrayField(new long[] { 1l, 2l });
		HashSet<Long> longSet = new HashSet<Long>();
		Arrays.asList(1l, 2l).addAll(longSet);
		manyTypes.setLongSetField(longSet);
		manyTypes.setCharArrayField(new char[] { 'a', 'b' });
		manyTypes.setCharacterArrayField(new Character[] { 'a', 'b' });
		return manyTypes;
	}

	@RequestMapping(value = "/many7/{id}", method = RequestMethod.GET)

	public ManyTypes getMany7Types(@PathVariable("id") @DateTimeFormat(pattern = "yyyy-MM-dd") java.time.LocalDate id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many8/{id}", method = RequestMethod.GET)

	public ManyTypes getMany8Types(@PathVariable("id") @DateTimeFormat(pattern = "HH:mm:ss") java.time.LocalTime id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many9/{id}", method = RequestMethod.GET)

	public ManyTypes getMany9Types(@PathVariable("id") Integer id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many10/{id}", method = RequestMethod.GET)

	public ManyTypes getMany10Types(@PathVariable("id") Long id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many11/{id}", method = RequestMethod.GET)

	public ManyTypes getMany11Types(@PathVariable("id") Float id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many12/{id}", method = RequestMethod.GET)

	public ManyTypes getMany12Types(@PathVariable("id") float id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many13/{id}", method = RequestMethod.GET)

	public ManyTypes getMany13Types(@PathVariable("id") double id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many14/{id}", method = RequestMethod.GET)

	public ManyTypes getMany14Types(@PathVariable("id") boolean id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

	@RequestMapping(value = "/many15/{id}", method = RequestMethod.GET)

	public ManyTypes getMany15Types(@PathVariable("id") Boolean id) {
		ManyTypes manyTypes = new ManyTypes();
		return manyTypes;
	}

}