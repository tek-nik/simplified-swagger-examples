package hello.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.entities.Person;
import hello.service.PersonService;

@RestController
public class PersistingControllerUsingValid {
	
	@Autowired
	PersonService personService;

	@RequestMapping(value = "/saveperson", method = RequestMethod.POST,  consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public Person save(@Valid @RequestBody Person y) {
		return personService.save(y);
	}
	
	@RequestMapping(value = "/listperson", method = RequestMethod.GET,
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<Person> list() {
		return personService.list();
	}

}