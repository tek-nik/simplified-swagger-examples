package hello.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.Person;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.Extension;
import io.swagger.annotations.ExtensionProperty;
import io.swagger.annotations.ResponseHeader;

@RestController
/*
 * An attempt is made to ignore (sometimes conditionally ignore) some @ApiOperation annotation attributes
 * because the data can be otherwise derived more accurately.
 * See readme.md
 */

public class ExampleApiOperationController {
	@RequestMapping(value = "/apiopabc", method = RequestMethod.POST,  consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
@ApiOperation(code=200, 
	
	extensions= {@Extension(name="extn1", properties = { 
			@ExtensionProperty(name = "advisor", value = "advisors are allowed getting every location") })},
	position=1, 
	
	//authorizations= {@Authorization(value = "oauth2-rop", scopes = {@AuthorizationScope(description = "scope2", scope = "scope1")}),
	//		@Authorization( value = "Bearer")}
	
			authorizations = { @Authorization(value="Authorization1") }

, 
	
	value="simple @ApiOperation demo",  
			notes = "Demonstrating usage of @ApiOperation. While the aim is to use as many attributes provided by @ApiOperation as is needed. Ignoring the attributes whose usage causes problems.",
			hidden=false,
			response=String.class,
			
	        produces = "application/hal+json, application/hal+json;concept=locations;v=1",
	        consumes = "application/hal+json, application/hal+json;concept=locations;v=1",
	        nickname = "nick1",
	        httpMethod= "PATCH",
	         ignoreJsonView=true,
	         protocols= "https,http",
	         responseContainer="Map",
	         responseHeaders=@ResponseHeader(name="haeder1",description="desc", response=String.class, 
	         responseContainer="List"),
	         responseReference="integer",
	         tags= {"tag1"}
	       
			)
	
	public Person apiopabc(@Valid @RequestBody Person x) {
		return x;
	}
	
	@RequestMapping(value = "/apiopabc1", method = RequestMethod.POST,  consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})

	
	public Person apiopabc1(@Valid @RequestBody Person x) {
		return x;
	}
	@RequestMapping(value = "/def1", method = RequestMethod.POST,  consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public Person def1(@Valid @RequestBody Person x) {
		return x;
	}
	
	
}
