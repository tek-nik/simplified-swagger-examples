package hello.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.Person;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
/*
 * An attempt is made to ignore (sometimes conditionally ignore) some @ApiResponse annotation attributes
 * because the data can be otherwise derived more accurately.
 * See readme.md
 */
public class ExampleApiResponsesController {
	@RequestMapping(value = "/apiresponses", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })

	
	@ApiResponses(value = { @ApiResponse(code = 200, message = "返回应用服务器的请求列表", response = String.class),
			@ApiResponse(code = 201, message = "返回错误信息", response = String.class) })
	public Person apiopabc(@Valid @RequestBody Person x) {
		return x;
	}
	
}
