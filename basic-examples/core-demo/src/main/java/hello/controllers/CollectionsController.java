package hello.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.BeanWithAMapProperty;
import io.swagger.annotations.ApiOperation;

@RestController

public class CollectionsController {
	
	
	 

	
	 @RequestMapping(value = "/abcmap/{id}", method = RequestMethod.POST)
	  
	  public Map getMap(@PathVariable("id") String id, @RequestBody Map x) {
	  
	  Map ret = x; ret.put("check", 10l); ret.put(id, 10l); return ret; }
	 

	
	@ApiOperation( value="simple list example", notes = "Not showing with a request body  of list and method type say POST.While the swagger docs generated will be good, the actual processing wont work unless the list is parameterized. For parameterized lists @see hello.controllers.GenericsControllerUsingValid#getList2().")	
	  
	  @RequestMapping(value = "/abclist/{id}", method = RequestMethod.GET)
	  
	  public List getList(@PathVariable("id") String id
	  /*, @RequestBody List x*/
	) {
	  List<String> ret = new ArrayList<>(); ret.add("check"); ret.add(id); return
	  ret; }
	  
	
	@RequestMapping(value = "/abcbeanwithmap/{id}", method = RequestMethod.POST)

	public BeanWithAMapProperty getbeanWithMap(@PathVariable("id") String id,  @RequestBody BeanWithAMapProperty x) {
		BeanWithAMapProperty<String, Long> ret=x;
		//BeanWithAMapProperty<String, Long> ret = new BeanWithAMapProperty<>();
		ret.setField1("abc");
		if(ret.getField2()==null)
		{
		Map<String, Long> map = new HashMap<>();
		ret.setField2(map);
		}
		ret.getField2().put(id, 1000l);
		
		return ret;
	}
	 
	 

}