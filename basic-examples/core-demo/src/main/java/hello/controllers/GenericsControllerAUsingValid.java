package hello.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.GenericBean1;
import hello.beans.GenericBeanA;

@RestController

public class GenericsControllerAUsingValid {

	
	
	@RequestMapping(value = "/ijk2", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBeanA<String, Integer> abc2(@Valid @RequestBody GenericBeanA<String, Integer> y) {
		return y;
	}
	
	@RequestMapping(value = "/ijk2", method = RequestMethod.GET,  
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBeanA<String, Integer> abc2() {
		GenericBeanA<String, Integer> y= new GenericBeanA<String, Integer>();
		y.setField1("filed1val");
		y.setField2(10);
		y.setGenericField1(new GenericBean1<String, Integer>("a", 1));
		y.setField3(new Integer[] {1,23,4});
		y.setGenericField2(new GenericBean1<String, Integer>("a", 1));
		
		 
		//yes this is not perfect because of type erasure
		GenericBean1<String, Integer>[] arr=new GenericBean1[] {new GenericBean1<String, Integer>("a", 1)};
		y.setGenericArray(arr);
		y.setGenericArray1(arr);
		return y;
	}
	
	@RequestMapping(value = "/ijk3", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBeanA<String, String> abc3(@Valid @RequestBody GenericBeanA<String, String> y) {
		return y;
	}
	
	@RequestMapping(value = "/ijk3", method = RequestMethod.GET,  
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public GenericBeanA<String, String> abc3() {
		GenericBeanA<String, String> y= new GenericBeanA<String, String>();
		y.setField1("filed1val");
		y.setField2("filed2val");
		y.setGenericField1(new GenericBean1<String, Integer>("a", 1));
		y.setField3(new String[] {"str1", "str2"});
		y.setGenericField2(new GenericBean1<String, String>("a", "b"));
		
		 
		//yes this is not perfect because of type erasure
		GenericBean1<String, Integer>[] arr=new GenericBean1[] {new GenericBean1<String, Integer>("a", 1)};
		y.setGenericArray(arr);
		GenericBean1<String, String>[] arr1=new GenericBean1[] {new GenericBean1<String, String>("a", "b")};
		y.setGenericArray1(arr1);
		return y;
	}
	
	
	
	
}