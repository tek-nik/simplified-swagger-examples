package hello.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans1.Person;

@RestController
@RequestMapping(path= {"/base", "base1"})
public class Hello1ControllerUsingValid {

	@RequestMapping(value = "/abc1", method = RequestMethod.POST,  consumes= {MediaType.APPLICATION_XML_VALUE,  MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	public Person abc1(@Valid @RequestBody hello.beans1.Person y) {
		return y;
	}

}