package hello.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans.Person;
@RestController

public class RequestEntityController {
	
	@RequestMapping(value = "/abcre", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person abc(@Valid  RequestEntity<Person> x) throws MethodArgumentNotValidException {
		/*
		 *  RequestEntity doesnt participate in validations.
		 *  Hence programmatic.
		 */
		programmaticValidation(x.getBody(), "body");
		return x.getBody();
	}

	private void programmaticValidation(Object target, String name) throws MethodArgumentNotValidException {
		LocalValidatorFactoryBean validator=new LocalValidatorFactoryBean();
		  validator.afterPropertiesSet();
		Errors errors = new BeanPropertyBindingResult(target, name);
		validator.validate(target, errors);
		if(errors.hasErrors())
		{
		 MethodArgumentNotValidException methodArgumentNotValidException = new MethodArgumentNotValidException(null, (BindingResult) errors);
		 throw  methodArgumentNotValidException;
		}
	}
	
	@RequestMapping(value = "/abcrb", method = RequestMethod.POST,  consumes= { MediaType.APPLICATION_JSON_VALUE},
			produces= {MediaType.APPLICATION_JSON_VALUE})
	
	public Person abc(@Valid  @RequestBody Person x) {
		return x;
	}

}
