package hello.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hello.beans2.ExampleApiModelPropertyBean;

@RestController
public class ExampleApiModelPropertyController {
	@RequestMapping(value = "/apimodelprop", method = RequestMethod.POST, consumes = { 
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })

	/**
	 * See the bean ExampleApiModelPropertyBean for the annotations.
	 * 
	 * 
	 * @param x
	 * @return
	 */
	public ExampleApiModelPropertyBean<String> apimodelprop(@Valid @RequestBody ExampleApiModelPropertyBean<String> x) {
		return x;
	}
}
