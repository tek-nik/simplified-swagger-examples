package hello.beans2;

import javax.validation.constraints.NotNull;

public class GeneriChildBean<E> {
	@NotNull
	private String field1;
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public E getGenericField1() {
		return genericField1;
	}
	public void setGenericField1(E genericField1) {
		this.genericField1 = genericField1;
	}
	private E genericField1;
}
