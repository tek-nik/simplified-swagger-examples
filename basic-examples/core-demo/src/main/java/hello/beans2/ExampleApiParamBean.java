package hello.beans2;
/*
 * An attempt is made to ignore (sometimes conditionally ignore) some @ApiParam annotation attributes
 * because the data can be otherwise derived more accurately.
 * See readme.md
 */
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
public class ExampleApiParamBean {
	private String field1;
	@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=false, 
			name="name2", 
			readOnly=false ,
			required=false, 
			type="typex", 
			value="val1")
	private String field2;
	@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=false, 
			name="name2", 
			readOnly=true ,
			required=false, 
			
			type="typex", 
			value="val1")
	private String field3;
	@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=true, 
			name="name2", 
			readOnly=false ,
			required=false, 
			type="typex", 
			value="val1")
	private String field4;
	@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=false, 
			name="name2", 
			readOnly=true ,
			required=false, 
			
			type="typex", 
			value="val1")
	@NotNull
	private String field5;
	@ApiParam(access="access1", 
			allowableValues= "1,2", 
			allowEmptyValue=false,
			allowMultiple=true,
			collectionFormat="csv",
			defaultValue="2",
			examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
			, example="4", 
			format="format1", 
			hidden=true, 
			name="name2", 
			readOnly=false ,
			required=false, 
			type="typex", 
			value="val1")
	@NotNull
	private String field6;
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	
	
}
