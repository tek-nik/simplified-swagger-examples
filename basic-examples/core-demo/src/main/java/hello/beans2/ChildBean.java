package hello.beans2;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiParam;

public class ChildBean {
	@NotNull
	//@Size(min=1)
	@ApiParam(readOnly=true, hidden=false)
	private String field1;

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

}
