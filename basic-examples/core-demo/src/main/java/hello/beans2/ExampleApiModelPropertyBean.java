package hello.beans2;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import io.swagger.annotations.Extension;
import io.swagger.annotations.ExtensionProperty;
@XmlRootElement(name="exampleModelPropertyBean")
@XmlAccessorType(XmlAccessType.FIELD) 

public class ExampleApiModelPropertyBean<E> {
	
	/*
	 * An attempt is made to ignore (sometimes conditionally ignore) some @ApiModelProperty annotation attributes
	 * because the data can be otherwise derived more accurately.
	 * See readme.md
	 */
	
	@ApiModelProperty(
			allowEmptyValue=true,
			dataType="mytype", 
			example="100", 
					 position=5,
							 reference="integer",
									 name="abc",
			  notes="not1",
			   value="val1",
					  hidden=false,
							  required=false,
			 extensions={@Extension(properties = 
			{ @ExtensionProperty(name = "xtn1", value = "xtnval1") },
			name="xtnname1")})
	private E field1;
	
	@ApiModelProperty(access="accesstring", 
			accessMode=AccessMode.READ_ONLY, 
			allowEmptyValue=true,
			dataType="mytype", 
			example="100", 
					 position=3,
							 reference="integer",
									 name="abc",
			  notes="not1",
			   value="val1",
					  hidden=false,
							  required=false,
			 extensions={@Extension(properties = 
			{ @ExtensionProperty(name = "xtn1", value = "xtnval1") },
			name="xtnname1")})
	private E field2;
	
	@ApiModelProperty(access="accesstring", 
			accessMode=AccessMode.READ_WRITE, 
			allowEmptyValue=true,
			dataType="mytype", 
			example="100", 
					 position=4,
							 reference="integer",
									 name="abc",
			  notes="not1",
			   value="val1",
					  hidden=true,
							  required=true,
			 extensions={@Extension(properties = 
			{ @ExtensionProperty(name = "xtn1", value = "xtnval1") },
			name="xtnname1")})
	private E field3;
	
	@ApiModelProperty(access="accesstring", 
			accessMode=AccessMode.READ_ONLY, 
			allowEmptyValue=true,
			dataType="mytype", 
			example="100", 
					 position=2,
							 reference="integer",
									 name="abc",
			  notes="not1",
			   value="val1",
					  hidden=false,
							  required=false,
			 extensions={@Extension(properties = 
			{ @ExtensionProperty(name = "xtn1", value = "xtnval1") },
			name="xtnname1")})
	@NotNull
	private E field4;
	
	@ApiModelProperty(access="accesstring", 
			accessMode=AccessMode.READ_WRITE, 
			allowEmptyValue=true,
			dataType="mytype", 
			example="100", 
					 position=1,
							 reference="integer",
									 name="abc",
			  notes="not1",
			   value="val1",
					  hidden=true,
							  required=true,
			 extensions={@Extension(properties = 
			{ @ExtensionProperty(name = "xtn1", value = "xtnval1") },
			name="xtnname1")})
	@NotNull
	private E field5;

	public E getField1() {
		return field1;
	}

	public void setField1(E field1) {
		this.field1 = field1;
	}

	public E getField2() {
		return field2;
	}

	public void setField2(E field2) {
		this.field2 = field2;
	}

	public E getField3() {
		return field3;
	}

	public void setField3(E field3) {
		this.field3 = field3;
	}

	public E getField4() {
		return field4;
	}

	public void setField4(E field4) {
		this.field4 = field4;
	}

	public E getField5() {
		return field5;
	}

	public void setField5(E field5) {
		this.field5 = field5;
	}

	
	
}
