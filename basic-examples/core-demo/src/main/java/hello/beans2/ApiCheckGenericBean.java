package hello.beans2;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.CreditCardNumber;

import hello.beans.Address;
import hello.beans1.TitleEnum;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import io.swagger.annotations.Extension;
import io.swagger.annotations.ExtensionProperty;
@XmlRootElement(name="person")
@XmlAccessorType(XmlAccessType.FIELD) 
public class ApiCheckGenericBean<E> {

	@ApiModelProperty(access="accesstring", 
			accessMode=AccessMode.READ_ONLY, 
			//allowableValues="1,2", 
			allowEmptyValue=true,
			dataType="mytype", 
			example="100", 
					 position=2,
							 reference="integer",
									 name="abc",
			  notes="not1",
			   value="val1",
					  hidden=true,
							  required=false,
			 extensions={@Extension(properties = 
			{ @ExtensionProperty(name = "xtn1", value = "xtnval1") },
			name="xtnname1")})
	private String fullName;
	@Valid
	@Size(min = 1, max = 2)
	@XmlElement
	private Set<Address> addresses;
	@CreditCardNumber
	private String creditCardNumber;
	@ApiModelProperty(access="accesstring", accessMode=AccessMode.AUTO,  
			allowableValues="1,2, 4, 5 ",
			//allowableValues="range[4, infinity], 1, 2",
			allowEmptyValue=true,
			dataType="mytype", example="100", extensions={@Extension(properties = 
		{ @ExtensionProperty(name = "xtn1", value = "xtnval1") }, name="xtnname1")}, 
			hidden=false, name="abc", notes="not1", position=1,
			readOnly=true, reference="integer", required=true, value="val1" )
	@NotNull
	private TitleEnum title;
	
	
	@ApiModelProperty(access="accesstring", accessMode=AccessMode.READ_ONLY,  
			allowableValues="1,2, 4, 5 ",
			//allowableValues="range[4, infinity], 1, 2",
			allowEmptyValue=true,
			dataType="mytype", example="100", extensions={@Extension(properties = 
		{ @ExtensionProperty(name = "xtn1", value = "xtnval1") }, name="xtnname1")}, 
			hidden=false, name="abc", notes="not1", position=1,
			readOnly=false, reference="integer", required=false, value="val1" )
	@NotNull
	private E genericField;

	public E getGenericField() {
		return genericField;
	}

	public void setGenericField(E genericField) {
		this.genericField = genericField;
	}

	public TitleEnum getTitle() {
		return title;
	}

	public void setTitle(TitleEnum title) {
		this.title = title;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
