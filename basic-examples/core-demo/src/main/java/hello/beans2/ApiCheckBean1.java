package hello.beans2;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiParam;

public class ApiCheckBean1 {
	
/*private GeneriChildBean<GeneriChildBean1<String>>[] genericChildBeanArray2;
	public GeneriChildBean<GeneriChildBean1<String>>[] getGenericChildBeanArray2() {
return genericChildBeanArray2;
}
public void setGenericChildBeanArray2(GeneriChildBean<GeneriChildBean1<String>>[] genericChildBeanArray2) {
this.genericChildBeanArray2 = genericChildBeanArray2;
}*/
	
/*	private GeneriChildBean<GeneriChildBean1<String>> genericChildBean2;
	
	public GeneriChildBean<GeneriChildBean1<String>> getGenericChildBean2() {
		return genericChildBean2;
	}
	public void setGenericChildBean2(GeneriChildBean<GeneriChildBean1<String>> genericChildBean2) {
		this.genericChildBean2 = genericChildBean2;
	}
	@NotNull
	private ChildBean childBean1;
	public ChildBean getChildBean1() {
		return childBean1;
	}
	public void setChildBean1(ChildBean childBean1) {
		this.childBean1 = childBean1;
	}*/
	/*private GeneriChildBean<String> genericChildBean1;*/
	
	@NotNull
	@Size(min=1)
	private ChildBean[] childBeanArray1;
	
	public ChildBean[] getChildBeanArray1() {
		return childBeanArray1;
	}
	public void setChildBeanArray1(ChildBean[] childBeanArray1) {
		this.childBeanArray1 = childBeanArray1;
	}
	/*
	 * private GeneriChildBean<String>[] genericChildBeanArray1;
	 * public GeneriChildBean<String>[] getGenericChildBeanArray1() {
		return genericChildBeanArray1;
	}
	public void setGenericChildBeanArray1(GeneriChildBean<String>[] genericChildBeanArray1) {
		this.genericChildBeanArray1 = genericChildBeanArray1;
	}

	private GeneriChildBean<String> genericChildBean1;
	
	public GeneriChildBean<String> getGenericChildBean1() {
		return genericChildBean1;
	}
	public void setGenericChildBean1(GeneriChildBean<String> genericChildBean1) {
		this.genericChildBean1 = genericChildBean1;
	}

	



		
	
	    
		public Set<String> getSetField1() {
		return setField1;
	}
	
		@NotNull
		@NotBlank
		@Size(min=2)
		private String stringField1;
		public String getStringField2() {
			return stringField2;
		}
		public void setStringField2(String stringField2) {
			this.stringField2 = stringField2;
		}
		public String getStringField3() {
			return stringField3;
		}
		public void setStringField3(String stringField3) {
			this.stringField3 = stringField3;
			public String getStringField1() {
			return stringField1;
		}
		public void setStringField1(String stringField1) {
			this.stringField1 = stringField1;
		}

		}*/
		//@ApiParam(readOnly=true)
	@NotNull
	@ApiParam(readOnly=true, hidden=true)
		private String stringField2;
		public String getStringField2() {
			return stringField2;
		}
		public void setStringField2(String stringField2) {
			this.stringField2 = stringField2;
		}

		/*@ApiParam(readOnly=false, allowMultiple=true,
				collectionFormat="csv")*/
		/*private String stringField3;
		private Date dateField1;
		private List<String> listField2;*/
		
	/*	@ApiParam(access="access1", 
				allowableValues= "1,2", 
				allowEmptyValue=false,
				allowMultiple=true,
				collectionFormat="csv",
				defaultValue="2",
				examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
				, example="4", 
				format="format1", 
				hidden=false, 
				name="name2", 
				readOnly=false ,//readonly does hide it
				required=false, 
				type="typex", 
				value="val1")*/
				
		/*private Set<String> setField1;
		private Set<String> setField2;*/
/*		@ApiParam(access="access1", 
				allowableValues= "1,2", 
				allowEmptyValue=false,
				allowMultiple=true,
				collectionFormat="csv",
				defaultValue="2",
				examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
				, example="4", 
				format="format1", 
				hidden=false, 
				name="name2", 
				readOnly=true ,
				required=false, 
				type="typex", 
				value="val1")*/
		/*String[] stringArray1;
		String[] stringArray2;
		public String[] getStringArray1() {
			return stringArray1;
		}
		public void setStringArray1(String[] stringArray1) {
			this.stringArray1 = stringArray1;
		}
		public String[] getStringArray2() {
			return stringArray2;
		}
		public void setStringArray2(String[] stringArray2) {
			this.stringArray2 = stringArray2;
		}
		
		
		public Date getDateField1() {
			return dateField1;
		}
		public void setDateField1(Date dateField1) {
			this.dateField1 = dateField1;
		}
		public List<String> getListField2() {
			return listField2;
		}
		public void setListField2(List<String> listField2) {
			this.listField2 = listField2;
		}
			
		public void setSetField1(Set<String> setField1) {
			this.setField1 = setField1;
		}
		public Set<String> getSetField2() {
			return setField2;
		}
		public void setSetField2(Set<String> setField2) {
			this.setField2 = setField2;
		}*/
/*		@ApiParam(access="access1", 
				allowableValues= "1,2", 
				allowEmptyValue=false,
				allowMultiple=true,
				collectionFormat="csv",
				defaultValue="2",
				examples= @Example(value = { @ExampleProperty(value = "abc", mediaType="mediaType1") })
				, example="4", 
				format="format1", 
				hidden=true, 
				name="name2", 
				readOnly=true ,
				required=false, 
				type="typex", 
				value="val1")*/
				/*private List<String> listField1;
		public List<String> getListField1() {
			return listField1;
		}
		public void setListField1(List<String> listField1) {
			this.listField1 = listField1;
		}
		
		private String string;
		private String string1;
		public String getString() {
			return string;
		}
		public void setString(String string) {
			this.string = string;
		}
		public String getString1() {
			return string1;
		}
		public void setString1(String string1) {
			this.string1 = string1;
		}
		*/
		
}