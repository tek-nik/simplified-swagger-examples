package hello.beans2;

public class GeneriChildBean1<E> {
	private int field1;
	
	public int getField1() {
		return field1;
	}
	public void setField1(int field1) {
		this.field1 = field1;
	}
	public E getGenericField1() {
		return genericField1;
	}
	public void setGenericField1(E genericField1) {
		this.genericField1 = genericField1;
	}
	private E genericField1;
}
