package hello.beans1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TitleEnum {
    Dr("Dr"),
    
    Sir("Sir"),
    
    Mr("Mr"),
    
    Ms("Ms"),
    
    Mrs("Mrs");

    private String value;

    TitleEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TitleEnum fromValue(String text) {
      for (TitleEnum b : TitleEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      //throw new IllegalArgumentException("Unexpected value '" + text + "'");
      return null;
    }
  }