package customvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import hello.beans.BeanWithCustomValidators;

public class ValidCrossFieldValidator implements ConstraintValidator<ValidCrossField, BeanWithCustomValidators> {

	@Override
	public boolean isValid(BeanWithCustomValidators input, ConstraintValidatorContext constraintContext) {

		if (input == null) {
			return true;
		}

		

		boolean isValid = true;
		
		if((input.getEmail()==null || input.getEmail().length()==0)&&input.getPhone()==null || input.getPhone().length()==0)
		{
			isValid=false;
			constraintContext.disableDefaultConstraintViolation();
			constraintContext.buildConstraintViolationWithTemplate("Need either phone or email.")
			.addPropertyNode("phone").addConstraintViolation();
			
		}
		
		return isValid;
	}
}
