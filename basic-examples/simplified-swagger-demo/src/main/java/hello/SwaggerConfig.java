package hello;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import hello.ver.VerConstant;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	ApiInfo apiInfo()
	{
		return new ApiInfo("Basic spring swagger examples", 
				"This basic examples project can be run in two modes- using regular swagger and using simplified swagger. Use the maven dependency spring-swagger-simplified for automatically creating additional documentation based on validation annotations. Please see Readme.md for details of all the fetures demonstrated.", 
				VerConstant.VERSION,
				"Use as you like",
				new Contact("Raghuraman Ramaswamy", "https://bitbucket.org/tek-nik/simplified-swagger-examples/", "raghu121d@gmail.com"),
				"The Apache Software License, Version 2.0",
				"http://www.apache.org/licenses/LICENSE-2.0.txt",
				Collections.EMPTY_LIST
				);
	}
	
	@Bean
	public Docket api() {
		
		return 
				new Docket(DocumentationType.SWAGGER_2)
					.select()
				.apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
				.paths(PathSelectors.any())
				
				.build().apiInfo(apiInfo());
				
			
	}
	
	


}