package hello;

import org.bitbucket.tek.nik.simplifiedswagger.SwaggerDecoratorConstants;
import org.bitbucket.tek.nik.simplifiedswagger.swaggerdecorators.ISwaggerDecorator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import hello.decorators.LocalDateTimeFormatSwaggerDecorator;
import hello.decorators.ValidCrossFieldSwaggerDecorator;


@Configuration
public class CustomAnnotationConfiguration {
	@Bean("customvalidators.LocalDateTimeFormat"+SwaggerDecoratorConstants.DECORATOR_SUFFIX)
	ISwaggerDecorator localDateTimeFormatSwaggerDecorator() {
		return new LocalDateTimeFormatSwaggerDecorator();
	}
	
	@Bean("customvalidators.ValidCrossField"+SwaggerDecoratorConstants.DECORATOR_SUFFIX)
	ISwaggerDecorator validCrossFieldSwaggerDecorator() {
		return new ValidCrossFieldSwaggerDecorator();
	}
	
}
