package hello.decorators;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;

import org.bitbucket.tek.nik.simplifiedswagger.newmodels.NewModelCreator;
import org.bitbucket.tek.nik.simplifiedswagger.swaggerdecorators.ISwaggerDecorator;

import customvalidators.ValidCrossField;
import io.swagger.models.Model;
import io.swagger.models.Operation;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.properties.Property;

public class ValidCrossFieldSwaggerDecorator implements ISwaggerDecorator {

	@Override
	public void decorateProperty(Property property, Annotation annotation,Class propertyType) {
		

	}

	

	@Override
	public void decorateParameter(Parameter parameter, Annotation annotation,java.lang.reflect.Parameter methodParameter) {
		
		
	}
	
	@Override
	public void decorateModel(Model model, Annotation annotation, Class modelClass) {
		ValidCrossField validCrossField = (ValidCrossField) annotation;
		populateVendorExtension(validCrossField, model.getVendorExtensions());
	}
	
	private void populateVendorExtension(ValidCrossField validCrossField, Map<String, Object> vendorExtensions) {
		vendorExtensions.put("validCrossField", true);
		
	}



	@Override
	public void decorateOperation(Operation operation, Annotation annotation, Method method, NewModelCreator newModelCreator) {
		
		
	}

}
